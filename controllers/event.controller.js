const { validationResult } = require('express-validator');
const { Event, User, Image } = require('../db/db');
const { logErrors } = require('../utils/validator_utils');

function readApi(req, res) {
  Event.findByPk(req.params.id, { include: [User, Image] })
    .then((event) => (event === null ? undefined : event.get({ plain: true })))
    .then((event) => {
      if (event) {
        res.json(event);
      } else {
        res.status(404).json({ message: 'A keresett rendezvény nem található.' });
      }
    });
}

function readAllApi(req, res) {
  const { location } = req.query;
  const eventsPromise = location ? Event.findAll({ where: { location } }) : Event.findAll();

  eventsPromise.then((events) => events.map((event) => event.get()))
    .then((events) => {
      res.json(events);
    });
}

function storeApi(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    res.status(422).json({ message: errors.array().map((error) => error.msg).join(' ') });
    return;
  }

  Event.create({
    name: req.body.name,
    startingDate: req.body.startingDate,
    endingDate: req.body.endingDate,
    location: req.body.location,
  })
    .then((result) => {
      const { id } = result.get();
      res.status(201).location(`/api/events/${id}`).json(result.get());
    });
}

function deleteApi(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    res.status(422).json({ message: 'A törölni kívánt rendezvény nem található!' });
    return;
  }

  Event.destroy({ where: { id: req.params.id } })
    .then(() => {
      res.sendStatus(204);
    });
}

function updateApi(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    res.status(422).json({ message: 'A módosítani kívánt rendezvény nem található!' });
    return;
  }

  Event.findByPk(req.params.id)
    .then((event) => {
      Object.assign(event, req.body);
      event.save()
        .then(() => {
          res.sendStatus(204);
        });
    });
}

async function joinApi(req, res) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    logErrors(errors);
    const message = errors.array().map((error) => error.msg).join(' ');
    res.status(422).json({ message });
    return;
  }

  const { eventId, action } = req.body;
  const { userId } = req.session;
  const event = await Event.findByPk(eventId);

  if (action === 'join') {
    User.findByPk(userId)
      .then((user) => {
        user.addEvent(event);

        console.log(`User ${userId} added to event ${eventId}`);
        res.json({ message: 'Sikeres csatlakozás.' });
      });
  } else {
    User.findByPk(userId)
      .then((user) => {
        user.removeEvent(event);

        console.log(`User ${userId} removed from event with ID ${eventId}`);
        res.json({ message: 'Sikeres kilépés.' });
      });
  }
}

function show(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    res.status(422).render('error', { message: 'A kért rendezvény nem található!' });
    return;
  }

  Event.findByPk(req.params.id, { include: [User, Image] })
    .then((event) => event.get({ plain: true }))
    .then((event) => {
      console.log(event);
      const canUpload =  Boolean(event.users.find((user) => user.id === req.session.userId));
      res.render('event/show', { event, canUpload, errors: req.flash('error') });
    });
}

function showAll(req, res) {
  Event.findAll({ include: [User] })
    .then((events) => events.map((event) => event.get({ plain: true })))
    .then((events) => {
      events.forEach((event) => {
        if (event.users.find((user) => user.id === req.session.userId)) {
          // eslint-disable-next-line no-param-reassign
          event.isOrganizer = true;
        }
      });
      return events;
    })
    .then((events) => {
      res.render('event/index', { events });
    });
}

function create(req, res) {
  res.render('event/create', { errors: req.flash('error') });
}

function store(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    req.flash('error', errors.array());
    res.status(422).redirect('/event/create');
    return;
  }

  Event.create(req.body)
    .then((event) => {
      console.log(`Event ${event.id} created`);
      res.redirect('/');
    });
}


module.exports = {
  readApi,
  readAllApi,
  storeApi,
  deleteApi,
  updateApi,
  joinApi,
  show,
  showAll,
  create,
  store,
};
