FROM node:15.3-alpine3.10

WORKDIR /usr/eventplanner

COPY package.json .
RUN npm i --production

COPY . .

EXPOSE 8080

CMD npm run production