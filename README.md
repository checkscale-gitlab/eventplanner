<div align="center">
  <h1 align="center">Event Planner</h1>

  <p align="center">
    Event Planner is a small web application for creating, finding and managing events.
    <br />
    <br />
    <a href="https://eventplanner-szilardtumo.herokuapp.com/">View Demo</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>


## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

Event Planner is a small web application for creating, finding and managing events. The application has two main roles: administrator and organizer(user).
The administrators are able to create new events, and the organizers can join to them. If joined, the organizer can upload photos to the specific event.


### Built With

* [Express.js](https://expressjs.com/)
* [Handlebars](https://handlebarsjs.com/)
* [Sequelize](http://sequelize.org/)


## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* npm

### Installation

1. Clone the repo
   ```sh
   git clone git@gitlab.com:tumoszilard/eventplanner.git
   ```
   or <br/>
   ```sh
   git clone https://gitlab.com/tumoszilard/eventplanner.git
   ```

2. Install NPM packages
   ```sh
   npm install
   ```


## Usage

### On the local machine

1. Create a `.env.development` file based on the `.env.example`.

2. Start the application:
    - in development mode: `npm start`.
    - in production mode: `npm run production`.


### With Docker

1. Run `docker-compose up`.

## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contact

Szilárd Tumó - tumoszilard@gmail.com

Project Link: [https://gitlab.com/tumoszilard/eventplanner](https://gitlab.com/tumoszilard/eventplanner)
