module.exports = (roles) => (req, res, next) => {
  const rolesArray = typeof roles === 'string' ? [roles] : roles;
  const currentRole = req.session.role || 'guest';

  if (rolesArray.includes(currentRole)) {
    next();
  } else {
    res.status(401).render('error', { message: 'Ehhez nincs jogosultságod!' });
  }
};
