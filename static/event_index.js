function getDetails(e) {
  const { id } = e.target.dataset;

  fetch(`/api/events/${id}`)
    .then((res) => ({ ok: res.ok, res: res.json() }))
    .then(({ ok, res }) => {
      if (!ok) {
        throw Error(res.message);
      }
      return res;
    })
    .then((event) => {
      if (e.target.nextElementSibling.classList.contains('organizer-title')) {
        e.target.nextSibling.remove();
        e.target.nextSibling.remove();
      }

      const title = document.createElement('p');
      title.classList.add('organizer-title');
      title.innerHTML = 'Szervezők:';
      e.target.parentNode.insertBefore(title, e.target.nextSibling);

      const list = document.createElement('ul');

      event.users.forEach((user) => {
        const element = document.createElement('li');
        element.innerHTML = user.fullName;
        list.appendChild(element);
      });

      e.target.parentNode.insertBefore(list, title.nextSibling);
    })
    .catch((err) => {
      console.log(err);
    });
}

function changeStatus(e) {
  const { id, action } = e.target.dataset;

  const data = {
    action,
    eventId: id,
  };
  fetch(`/api/events/${id}/action`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  })
    .then((res) => ({ ok: res.ok, status: res.status, res: res.json() }))
    .then(({ ok, status, res }) => {
      if (status === 401) {
        throw Error('Nem megengedett művelet!');
      }
      if (!ok) {
        throw Error(res.message);
      }
      return res;
    })
    .then((res) => res.json())
    .then((res) => {
      if (action === 'join') {
        e.target.value = 'Kilépés';
        e.target.dataset.action = 'quit';
      } else {
        e.target.value = 'Csatlakozás';
        e.target.dataset.action = 'join';
      }

      if (e.target.previousElementSibling.classList.contains('alert')) {
        e.target.previousElementSibling.remove();
      }

      const flash = document.createElement('div');
      flash.classList.add('alert');
      flash.classList.add('alert-success');
      flash.innerHTML = res.message;

      e.target.parentNode.insertBefore(flash, e.target);
    })
    .catch((err) => {
      console.log(err);

      if (e.target.previousElementSibling.classList.contains('alert')) {
        e.target.previousElementSibling.remove();
      }

      const flash = document.createElement('div');
      flash.classList.add('alert');
      flash.classList.add('alert-warning');
      flash.innerHTML = err.message;

      e.target.parentNode.insertBefore(flash, e.target);
    });
}

const eventNames = document.querySelectorAll('.event-name');
const eventButtons = document.querySelectorAll('.event-button');

eventNames.forEach((eventName) => {
  eventName.addEventListener('click', getDetails);
});

eventButtons.forEach((eventButton) => {
  eventButton.addEventListener('click', changeStatus);
});
