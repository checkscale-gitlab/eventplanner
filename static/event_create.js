const endingDateInput = document.querySelector('#ending-date');
const startingDateInput = document.querySelector('#starting-date');

function isEndingDateValid() {
  return new Date(startingDateInput.value) < new Date(endingDateInput.value);
}

const submitButton = document.querySelector('#submit');

submitButton.addEventListener('click', () => {
  if (!isEndingDateValid()) {
    endingDateInput.setCustomValidity('A befejezési dátum a kezdési dátum után kell legyen');
  } else {
    endingDateInput.setCustomValidity('');
  }
});
