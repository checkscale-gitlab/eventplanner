module.exports = (sequelize, Sequelize) => sequelize.define('user', {
  username: {
    type: Sequelize.STRING,
    unique: true,
  },
  fullName: Sequelize.STRING,
  password: Sequelize.STRING,
  role: Sequelize.STRING,
});
