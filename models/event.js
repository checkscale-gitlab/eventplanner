module.exports = (sequelize, Sequelize) => sequelize.define('event', {
  name: {
    type: Sequelize.STRING,
  },
  startingDate: {
    type: Sequelize.DATEONLY,
  },
  endingDate: {
    type: Sequelize.DATEONLY,
  },
  location: {
    type: Sequelize.STRING,
  },
});
