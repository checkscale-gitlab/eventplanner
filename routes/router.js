const express = require('express');
const formidable = require('express-formidable');
const config = require('../config');
const validate = require('../middleware/validate');
const eventController = require('../controllers/event.controller');
const imageController = require('../controllers/image.controller');
const authController = require('../controllers/auth.controller');
const authorize = require('../middleware/authorize');

const router = express.Router();

router.get('/login', authorize('guest'), authController.showLoginForm);
router.get('/logout', authorize(['user', 'admin']), authController.logout);
router.get('/register', authorize('guest'), authController.showRegistrationForm);
router.post('/login', authorize('guest'), validate('login'), authController.login);
router.post('/register', authorize('guest'), validate('register'), authController.register);
router.get('/', eventController.showAll);
router.get('/event/create', authorize('admin'), eventController.create);
router.get('/event/:id', validate('showEvent'), eventController.show);
router.post('/event/store', authorize('admin'), validate('createEvent'), eventController.store);
router.post('/image/upload',
  authorize(['user', 'admin']),
  formidable({ uploadDir: config.app.uploadDir }),
  validate('uploadImage'),
  imageController.store);

module.exports = router;
