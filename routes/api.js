const express = require('express');
const bodyParser = require('body-parser');
const validate = require('../middleware/validate');
const authorize = require('../middleware/authorize');
const eventController = require('../controllers/event.controller');

const router = express.Router();
router.use(bodyParser.json());

router.get('/events/:id', validate('showEvent'), eventController.readApi);
router.post('/events/:id/action', authorize(['user', 'admin']), validate('joinEvent'), eventController.joinApi);

router.get('/events', eventController.readAllApi);
router.post('/events', validate('createEvent'), eventController.storeApi);
router.delete('/events/:id', validate('deleteEvent'), eventController.deleteApi);
router.patch('/events/:id', validate('updateEvent'), eventController.updateApi);

module.exports = router;
